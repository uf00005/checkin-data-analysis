import pandas as pd
import checkin

# Load in the data
checkins = pd.read_csv("data/checkins.csv")
users = pd.read_csv("data/users.csv")
venues = pd.read_csv("data/venues.csv")
categories = pd.read_csv("data/categories.csv")
venue_categories = pd.read_csv("data/venuecategoriesbasic.csv")

# Link the tables
checkins = checkins.join(venue_categories.set_index("venue"), on="venueid")

# Calculate the number of checkins for each user
users["count"] = checkin.get_checkin_counts(users, checkins)

# Get the users that have more than t checkins
def get_top_users(users, t):
    if "count" not in users.columns:
        raise ValueError("No user counts under 'count' header")
    return users[users.count > t]